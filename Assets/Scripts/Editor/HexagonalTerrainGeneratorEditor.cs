using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.TerrainTools;
using UnityEngine;

[CustomEditor(typeof(HexagonalTerrainGenerator))]
public class HexagonalTerrainGeneratorEditor : Editor
{
    //string indexText;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        //indexText = GUILayout.TextField("cane", 25);
        //if (GUILayout.Button("DebugNeighbour"))
        //{
        //    if (indexText != null && int.TryParse(indexText, out int inputIndex))
        //    {
        //        var terrainGenerator = target as HexagonalTerrainGenerator;
        //        terrainGenerator.DebugGetNeighbours(inputIndex);
        //    }
        //}
        if (GUILayout.Button("Generate"))
        {
            var terrainGenerator = target as HexagonalTerrainGenerator;
            terrainGenerator.CalculateHexagonTerrain();
        }

        if (GUILayout.Button("AssignNeighbours"))
        {
            var terrainGenerator = target as HexagonalTerrainGenerator;
            terrainGenerator.AssignNeighbours();
        }

        if (GUILayout.Button("Color"))
        {
            var terrainGenerator = target as HexagonalTerrainGenerator;
            terrainGenerator.ColorMesh();
        }
    }
}
