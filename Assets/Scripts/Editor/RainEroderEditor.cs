using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(RainEroder))]
public class RainEroderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Erode"))
        {
            var eroder = target as RainEroder;
            eroder.Erode();
        }
    }
}
