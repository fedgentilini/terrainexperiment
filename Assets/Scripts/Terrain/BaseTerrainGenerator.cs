using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class BaseTerrainGenerator : MonoBehaviour
{
    public static readonly string PATH = "/Islands/";
    public static readonly string PREFIX = "/Island_";
    public const float SMOOTHFACTOR = 0.05f;
    public const float MAXNOISEOFFSET = 16384;
    //Chance
    public const float COMMONCHANCE = 0.5f;
    public const float UNCOMMONCHANCE = 0.3f;
    public const float RARECHANCE = 0.15f;
    public const float EPICCHANCE = 0.04f;
    public const float LEGENDARYCHANCE = 0.01f;
    //
    public const float COMMONRADIUS = 12.5f;
    public const float UNCOMMONRADIUS = 25;
    public const float RARERADIUS = 38f;
    public const float EPICRADIUS = 50f;
    public const float LEGENDARYRADIUS = 63f;
    //
    public const float TERRAINSIZE = 140f;
    public const float CELLSIZE = 45f;
    public Action TerrainDrew;
    public Action PictureCaptured;

    [Header("Seed")]
    [SerializeField]
    public int seed = 0;
    protected float _seededOffset;
    protected Mesh _terrainMesh;
    protected private Mesh waterMesh;
    [Header("Terrain mesh sizes")]
    [SerializeField]
    protected float _verticesDensity = 1;
    protected float _verticesDistance = 0.25f;
    [SerializeField]
    protected float _terrainSize = 8;
    protected float _halfSize;
    [SerializeField]
    protected int _gridSize;
    [SerializeField]
    protected float _maxHeight = 1;
    protected Vector3[] _meshVertices;
    protected Color[] _verticesColors;
    [SerializeField]
    protected AnimationCurve curve;
    protected float[] _heights;
    protected TerrainVertex[] _terrainVertices;

    [Header("Terrain materials")]
    protected BiomeCore[] _islandBiomeCores;
    protected Vector2[] _uv;
    [SerializeField]
    protected Material _mainMaterial;
    public float SeededOffset { get => _seededOffset; set => _seededOffset = value; }
    public float TerrainSize { get => _terrainSize; set => _terrainSize = value; }
    public float MaxMainHeight { get => _maxHeight; set => _maxHeight = value; }
    public int GridSize { get => _gridSize; set => _gridSize = value; }
    public TerrainVertex[] TerrainVertices { get => _terrainVertices; set => _terrainVertices = value; }
    public Vector3[] MeshVertices { get => _meshVertices; set => _meshVertices = value; }
    public Mesh TerrainMesh { get => _terrainMesh; set => _terrainMesh = value; }
    public BiomeCore[] IslandBiomeCores { get => _islandBiomeCores; set => _islandBiomeCores = value; }

    protected virtual void Awake()
    {
        _terrainSize = TERRAINSIZE;
    }

    public float RemapValue(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

    public void CamCapture()
    {
        Texture2D Image = new(Camera.main.pixelWidth,
                Camera.main.pixelHeight, TextureFormat.RGB24, false);
        StartCoroutine(UpdateScreenshotTexture(Image));
    }

    public IEnumerator UpdateScreenshotTexture(Texture2D Image)
    {
        yield return new WaitForEndOfFrame();
        RenderTexture transformedRenderTexture = null;
        RenderTexture renderTexture = RenderTexture.GetTemporary(
            Camera.main.pixelWidth,
            Camera.main.pixelHeight,
            24,
            RenderTextureFormat.ARGB32,
            RenderTextureReadWrite.Default,
            1);
        try
        {
            ScreenCapture.CaptureScreenshotIntoRenderTexture(renderTexture);
            transformedRenderTexture = RenderTexture.GetTemporary(
                Camera.main.pixelWidth,
                Camera.main.pixelHeight,
                24,
                RenderTextureFormat.ARGB32,
                RenderTextureReadWrite.Default,
                1);
            Graphics.Blit(
                renderTexture,
                transformedRenderTexture,
                new Vector2(1.0f, -1.0f),
                new Vector2(0.0f, 1.0f));
            RenderTexture.active = transformedRenderTexture;
            Image.ReadPixels(
                new Rect(0, 0, Camera.main.pixelWidth, Camera.main.pixelHeight),
                0, 0);
        }
        catch (Exception e)
        {
            Debug.Log("Exception: " + e);
            yield break;
        }
        finally
        {
            RenderTexture.active = null;
            RenderTexture.ReleaseTemporary(renderTexture);
            if (transformedRenderTexture != null)
            {
                RenderTexture.ReleaseTemporary(transformedRenderTexture);
            }
        }
        var Bytes = Image.EncodeToJPG();
        Image.Apply();
        Destroy(Image);
        string filePath = "D:/Mega/" + PATH + PREFIX + "_" + seed;
        for (int i = 0; i < _islandBiomeCores.Length; i++)
        {
            filePath += "_" + _islandBiomeCores[i].biomeType;
        }

        filePath += ".jpg";
        File.WriteAllBytes(filePath, Bytes);
        PictureCaptured?.Invoke();
    }

    protected int[] TriangulateGrid()
    {
        int[] triangles = new int[(_gridSize) * (_gridSize) * 6];
        for (int row = 0; row < _gridSize; row++)
        {
            for (int col = 0; col < _gridSize; col++)
            {
                int baseIndex = row * (_gridSize) * 6 + col * 6;
                triangles[baseIndex] = col + row * (_gridSize + 1);
                triangles[baseIndex + 3] = triangles[baseIndex + 2] = col + (row * (_gridSize + 1)) + 1;
                triangles[baseIndex + 4] = triangles[baseIndex + 1] = col + (row + 1) * (_gridSize + 1);
                triangles[baseIndex + 5] = col + ((row + 1) * (_gridSize + 1)) + 1;
            }
        }

        return triangles;
    }

    public void ApplyMeshToRenderAndCollider()
    {
        GetComponent<MeshFilter>().mesh = _terrainMesh;
        GetComponent<MeshRenderer>().material = _mainMaterial;
        GetComponent<MeshCollider>().sharedMesh = _terrainMesh;

        MeshCollider collider = GetComponent<MeshCollider>();
        if (collider == null)
        {
            collider = gameObject.AddComponent<MeshCollider>();
        }

        collider.sharedMesh = _terrainMesh;
    }

    public void DrawMesh(Vector3[] vertices, int[] triangles)
    {
        _terrainMesh = new Mesh();
        _terrainMesh.name = "HexTerrain";
        _terrainMesh.vertices = vertices;
        _terrainMesh.triangles = triangles;
        _terrainMesh.RecalculateNormals();
    }

    public virtual void ColorMesh()
    {
       //
        _terrainMesh.colors = _verticesColors;
    }
    public Vector2[] GetCircleOffsetOnGrid(float radius)
    {
        List<Vector2> offsets = new List<Vector2>();

        int externalgridSize = Mathf.CeilToInt(radius / _verticesDistance);
        for (int i = 0; i < externalgridSize; i++)
        {
            for (int j = 0; j < externalgridSize; j++)
            {
                if (Vector2.Distance(Vector2.zero, new Vector2(i * _verticesDistance, j * _verticesDistance)) < radius)
                {
                    if (i != 0 || j != 0)
                    {
                        offsets.Add(new Vector2(i, j));
                    }

                    if (i != 0)
                    {
                        offsets.Add(new Vector2(-i, j));
                    }

                    if (j != 0)
                    {
                        offsets.Add(new Vector2(i, -j));
                    }

                    if (i != 0 && j != 0)
                    {
                        offsets.Add(new Vector2(-i, -j));
                    }
                }
            }
        }
        return offsets.ToArray();
    }

    public virtual void AssignNeighbours()
    {
    
    }
}

[Serializable]
public struct TerrainVertex
{
    public int index;
    public int triangularRingLevel;
    public bool isExternal;
    public bool isAngular;
    public int exantIndex;
    public int[] neighbourIndices;
    public Vector3 position;
    public float[] biomesWeights;
    public float distributionValue;

    public TerrainVertex(int index, Vector3 position, int triangularRingLevel = 0, bool isExternal = false, bool isAngular= false,int exantIndex = 0, float decorationValue = 0, int[] neighbourIndices = null, float[] biomesWeights = null)
    {
        this.index = index;
        this.triangularRingLevel = triangularRingLevel;
        this.isExternal = isExternal;
        this.isAngular = isAngular;
        this.exantIndex = exantIndex;
        this.position = position;
        this.distributionValue = decorationValue;
        this.neighbourIndices = neighbourIndices;
        this.biomesWeights = biomesWeights;
    }
}

[Serializable]
public enum BiomeType
{
    DESERT = 0,
    JUNGLE = 1,
    FOREST = 2,
    MARINE = 3,
    TUNDRA = 4
}

[Serializable]
public struct BiomeCore
{
    [HideInInspector]
    public Vector3 localPosition;
    public BiomeType biomeType;
}