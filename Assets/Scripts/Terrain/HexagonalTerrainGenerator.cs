using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class HexagonalTerrainGenerator : BaseTerrainGenerator
{
    const float BIOMEWEIGHTBLENDLOWERBOUND = 0.15f;
    const float BIOMEWEIGTHBLENDUPPERBOUND = 0.5f;
    [SerializeField]
    protected int _hexagonVerticesDensity = 10;
    protected float _islandCellTriangleHeight;

    [Header("Terrain Parameters")]
    [SerializeField]
    protected float _noiseBaseFrequency = 1f;
    [SerializeField]
    protected float _lacunarity = 0.5f;
    [SerializeField]
    protected float _persistence = 2f;
    [SerializeField]
    protected int _octaves = 3;
    [SerializeField]
    protected Color grassColor;
    [SerializeField]
    protected Color rockColor;
    [SerializeField]
    protected Color mountainColor;
    [SerializeField]
    protected float grassSlopeLimit;
    [SerializeField]
    protected float mountainHeightThereshold;


    [Header("Island Parameters")]
    protected float _islandRadius = 1;
    [SerializeField]
    protected float _beachLength = 0.9f;
    [SerializeField]
    protected float _fjordFrequency = 2;
    protected float _beachMaxOffsetRatio = (1f / 3);
    [SerializeField]
    protected float _seaBedShoreDistance = 1f;
    [SerializeField]
    protected float _seaBedDepth = 0.5f;

    HexagonCell[] _hexagonCells;

    [Header("Terrain materials")]
    [SerializeField]
    private Dictionary<BiomeType, Color> biomeColors;
    [SerializeField]
    protected float _biomeNoiseLacunarity;
    [SerializeField]
    protected float _biomeNoiseAmplitude;

    [SerializeField]
    protected TerrainType currentTerrain;

    public float IslandRadius { get => _islandRadius; set => _islandRadius = value; }
    public float BeachLength { get => _beachLength; set => _beachLength = value; }
    public HexagonCell[] HexagonCells { get => _hexagonCells; set => _hexagonCells = value; }

    public void CalculateHexagonTerrain()
    {
        DestroyImmediate(_terrainMesh);
        if (_hexagonCells == null || _hexagonCells.Length == 0)
        {
            _hexagonCells = new HexagonCell[_gridSize * _gridSize];
        }

        UnityEngine.Random.InitState(seed);
        //PlaceBiomeCores();
        _seededOffset = UnityEngine.Random.Range(0, MAXNOISEOFFSET);
        _halfSize = _terrainSize / 2;
        _islandCellTriangleHeight = (Mathf.Sqrt(3f) / 2) * _halfSize;
        _verticesDistance = _halfSize / _hexagonVerticesDensity;
        List<Vector3> hexagonVertices = new List<Vector3>();
        List<TerrainVertex> hexagonTerrainVertices = new List<TerrainVertex>();
        Quaternion rotation = Quaternion.Euler(0, 60, 0);
        int startingIndex = 1;
        int previousStartingIndex = 0;
        List<int> hexagonTriangles = new List<int>();
        Vector3 centralVertex = new Vector3(0, CalculateVertexHeight(0f, 0f), 0);
        hexagonVertices.Add(centralVertex);
        TerrainVertex terrainVertex = new TerrainVertex(hexagonTerrainVertices.Count, centralVertex, 0, false, true, 0);
        hexagonTerrainVertices.Add(terrainVertex);
        _hexagonCells[0] = new HexagonCell(Vector3.zero);
        Queue<int> indices = new Queue<int>();
        indices.Enqueue(0);
        for (int i = 1; i <= _hexagonVerticesDensity; i++) //hexagon rings
        {
            for (int j = 0; j < 6; j++) // sixths of the hexagon, hexant
            {
                Vector3 circleSixthStartingVertex = Quaternion.Euler(0, 60 * j, 0) * new Vector3(_verticesDistance * i, 0, 0);
                circleSixthStartingVertex = new Vector3(circleSixthStartingVertex.x, CalculateVertexHeight(circleSixthStartingVertex.x, circleSixthStartingVertex.z), circleSixthStartingVertex.z);
                hexagonVertices.Add(circleSixthStartingVertex);
                terrainVertex = new TerrainVertex(hexagonTerrainVertices.Count, circleSixthStartingVertex, i, i == _hexagonVerticesDensity, true, j);
                indices.Enqueue(hexagonTerrainVertices.Count);
                hexagonTerrainVertices.Add(terrainVertex);
                for (int k = 1; k < i; k++) // hexant subdivision
                {
                    Vector3 nextVert = rotation * circleSixthStartingVertex;
                    Vector3 direction = (nextVert - circleSixthStartingVertex).normalized;
                    Vector3 subVertex = circleSixthStartingVertex + direction * _verticesDistance * k;
                    subVertex = new Vector3(subVertex.x, CalculateVertexHeight(subVertex.x, subVertex.z), subVertex.z);
                    hexagonVertices.Add(subVertex);
                    terrainVertex = new TerrainVertex(hexagonTerrainVertices.Count, subVertex, i, i == _hexagonVerticesDensity, false, j);
                    indices.Enqueue(hexagonTerrainVertices.Count);
                    hexagonTerrainVertices.Add(terrainVertex);
                }
            }

            int circleVerticesNumber = hexagonVertices.Count - startingIndex;
            for (int j = 0; j < 6; j++)
            {
                if (i == 1) //first order hexagon sixth of circle
                {
                    hexagonTriangles.Add(startingIndex + j);
                    hexagonTriangles.Add(startingIndex + ((j + 1) % circleVerticesNumber));
                    hexagonTriangles.Add(0);
                }
                else
                {
                    int lowerCircleLength = 6 * (i - 1);
                    int mainSixthLowerIndex = startingIndex - (lowerCircleLength - (i - 1) * j);
                    for (int k = 0; k < i; k++)
                    {
                        if (k == 0)
                        {
                            hexagonTriangles.Add(startingIndex + (i * j));
                            hexagonTriangles.Add(startingIndex + (i * j) + 1);
                            hexagonTriangles.Add(mainSixthLowerIndex);
                        }
                        else
                        {
                            int outerVertexIndex = startingIndex + (i * j) + k;
                            int innerVertexIndex = ((mainSixthLowerIndex + k - previousStartingIndex) % lowerCircleLength) + previousStartingIndex;

                            hexagonTriangles.Add(outerVertexIndex);
                            hexagonTriangles.Add(innerVertexIndex);
                            hexagonTriangles.Add(mainSixthLowerIndex + k - 1);

                            hexagonTriangles.Add(outerVertexIndex);
                            hexagonTriangles.Add(startingIndex + ((i * j) + k + 1) % circleVerticesNumber);
                            hexagonTriangles.Add(innerVertexIndex);
                        }
                    }
                }
            }

            previousStartingIndex = startingIndex;
            startingIndex += (6 * i);
        }

        _terrainVertices = hexagonTerrainVertices.ToArray();

        DrawMesh(hexagonVertices.ToArray(), hexagonTriangles.ToArray());

        _hexagonCells[0].verticesIndices = indices.ToArray();
        ColorMesh();
        ApplyMeshToRenderAndCollider();

        TerrainDrew?.Invoke();
    }

    public override void ColorMesh()
    {
        _verticesColors = new Color[_terrainMesh.vertices.Length];

        for (int i = 0; i < _terrainMesh.vertices.Length; i++)
        {
            if (Vector3.Dot(_terrainMesh.normals[i], Vector3.up) < grassSlopeLimit)
            {
                _verticesColors[i] = rockColor;
            }
            else if (_terrainMesh.vertices[i].y > mountainHeightThereshold)
            {
                _verticesColors[i] = mountainColor;
            }
            else
            {
                _verticesColors[i] = grassColor;
            }
        }

        base.ColorMesh();
    }

    public void CalculateHexagonsGridTerrain(int islandCells)
    {
        DestroyImmediate(_terrainMesh);
        UnityEngine.Random.InitState(seed);
        _hexagonCells = new HexagonCell[_gridSize * _gridSize];

        if (islandCells == 1)
        {
            CalculateHexagonTerrain();
            return;
        }

        _terrainSize = CELLSIZE;

        PlaceBiomeCores();
        _seededOffset = UnityEngine.Random.Range(0, MAXNOISEOFFSET);
        _halfSize = _terrainSize / 2;
        _islandCellTriangleHeight = (Mathf.Sqrt(3f) / 2) * _halfSize;
        Vector3 cellStartingOffset = ((int)islandCells * 1.5f * _halfSize) * (Quaternion.Euler(0, -30, 0) * Vector3.forward);
        Vector3 horizontalGridOffset = 2 * _islandCellTriangleHeight * (Quaternion.Euler(0, -60, 0) * Vector3.back);
        Vector3 verticalGridOffset = 2 * _islandCellTriangleHeight * Vector3.back;
        Vector3[] cellsOffsets = new Vector3[_gridSize * _gridSize];
        _verticesDistance = _halfSize / _hexagonVerticesDensity;
        Queue<Vector3> hexagonsVertices = new Queue<Vector3>();
        Queue<TerrainVertex> hexagonTerrainVertices = new Queue<TerrainVertex>();
        Queue<int> hexagonTriangles = new Queue<int>();
        Quaternion rotation = Quaternion.Euler(0, 60, 0);
        int startingCircleIndex = 1;
        int previousStartingCircleIndex = 0;
        Vector3 centralVertex = new Vector3(0, 0, 0);
        hexagonsVertices.Enqueue(centralVertex);

        for (int i = 1, l = 0; i <= _hexagonVerticesDensity; i++) //hexagon circles
        {
            for (int j = 0; j < 6; j++) // hexagon sixths of circle
            {
                Vector3 circleSixthStartingVertex = Quaternion.Euler(0, 60 * j, 0) * new Vector3(_verticesDistance * i, 0, 0);
                circleSixthStartingVertex = new Vector3(circleSixthStartingVertex.x, 0, circleSixthStartingVertex.z);
                hexagonsVertices.Enqueue(circleSixthStartingVertex);
                for (int k = 1; k < i; k++, l++) // hexagon sixth subdivision
                {
                    Vector3 nextVert = rotation * circleSixthStartingVertex;
                    Vector3 direction = (nextVert - circleSixthStartingVertex).normalized;
                    Vector3 subVertex = circleSixthStartingVertex + direction * _verticesDistance * k;
                    subVertex = new Vector3(subVertex.x, 0, subVertex.z);
                    hexagonsVertices.Enqueue(subVertex);
                }
            }

            int circleVerticesNumber = hexagonsVertices.Count - startingCircleIndex;
            for (int j = 0; j < 6; j++)
            {
                if (i == 1) //first order hexagon sixth of circle
                {
                    hexagonTriangles.Enqueue(startingCircleIndex + j);
                    hexagonTriangles.Enqueue(startingCircleIndex + ((j + 1) % circleVerticesNumber));
                    hexagonTriangles.Enqueue(0);
                }
                else
                {
                    int lowerCircleLength = 6 * (i - 1);
                    int mainSixthLowerIndex = startingCircleIndex - ((lowerCircleLength - ((i - 1) * j)));
                    for (int k = 0; k < i; k++)
                    {
                        if (k == 0)
                        {
                            hexagonTriangles.Enqueue(startingCircleIndex + (i * j));
                            hexagonTriangles.Enqueue(startingCircleIndex + (i * j) + 1);
                            hexagonTriangles.Enqueue(mainSixthLowerIndex);
                        }
                        else
                        {
                            int outerVertexIndex = startingCircleIndex + (i * j) + k;
                            int innerVertexIndex = ((mainSixthLowerIndex + k - previousStartingCircleIndex) % lowerCircleLength) + previousStartingCircleIndex;

                            hexagonTriangles.Enqueue(outerVertexIndex);
                            hexagonTriangles.Enqueue(innerVertexIndex);
                            hexagonTriangles.Enqueue(mainSixthLowerIndex + k - 1);

                            hexagonTriangles.Enqueue(outerVertexIndex);
                            hexagonTriangles.Enqueue(startingCircleIndex + ((i * j) + k + 1) % circleVerticesNumber);
                            hexagonTriangles.Enqueue(innerVertexIndex);
                        }
                    }
                }
            }

            previousStartingCircleIndex = startingCircleIndex;
            startingCircleIndex += (6 * i);
        }


        Vector3[] baseVertices = hexagonsVertices.ToArray();
        int[] baseTriangles = hexagonTriangles.ToArray();
        hexagonsVertices = new Queue<Vector3>();
        List<TerrainVertex> outerCircleVerticesList = new List<TerrainVertex>();
        float distanceTollerance = _verticesDistance * 0.25f;
        int[] cellVerticesAliases;

        for (int i = 0, index = 0; i < _gridSize; i++)
        {
            for (int j = 0; j < _gridSize; j++, index++)
            {
                Vector3 cellOffset = cellStartingOffset + j * horizontalGridOffset + i * verticalGridOffset;
                _hexagonCells[index].mainBiome = GetClosestBiomes(cellOffset, _islandBiomeCores)[0];
                _hexagonCells[index].centerOffset = cellOffset;
                _hexagonCells[index] = new HexagonCell(cellOffset);
                cellsOffsets[i * _gridSize + j] = cellOffset;
                cellVerticesAliases = new int[baseVertices.Length];
                Queue<int> newCellVerticesIndices = new Queue<int>();
                for (int k = 0; k < baseVertices.Length; k++)
                {
                    Vector3 newVert = baseVertices[k] + cellOffset;
                    if (i != 0 || j != 0)
                    {
                        bool isVertexDuplicate = false;
                        int samePositionIndex = 0;
                        for (int l = 0; l < outerCircleVerticesList.Count; l++)
                        {
                            TerrainVertex outerVertex = outerCircleVerticesList[l];
                            if ((Vector2.Distance(new Vector2(outerVertex.position.x, outerVertex.position.z), new Vector2(newVert.x, newVert.z)) < distanceTollerance))
                            {
                                isVertexDuplicate = true;
                                samePositionIndex = outerVertex.index;
                            }
                        }

                        //check if a vertex is shared with another exagonCell
                        if (isVertexDuplicate)
                        {
                            cellVerticesAliases[k] = samePositionIndex;
                            newCellVerticesIndices.Enqueue(samePositionIndex);
                        }
                        else
                        {
                            cellVerticesAliases[k] = hexagonsVertices.Count;
                            newVert.y = CalculateIslandHeightWithShoreNoise(newVert.x, newVert.z);
                            newCellVerticesIndices.Enqueue(hexagonsVertices.Count);
                            hexagonsVertices.Enqueue(newVert);
                            float[] biomesWeights = GetVertexBiomesNoisedWeigths(newVert);
                            TerrainVertex terrainVertex = new TerrainVertex(hexagonTerrainVertices.Count, newVert);
                            terrainVertex.biomesWeights = biomesWeights;
                            hexagonTerrainVertices.Enqueue(terrainVertex);
                            if (k >= startingCircleIndex)
                            {
                                outerCircleVerticesList.Add(terrainVertex);
                            }
                        }
                    }
                    else
                    {
                        newVert.y = CalculateIslandHeightWithShoreNoise(newVert.x, newVert.z);
                        newCellVerticesIndices.Enqueue(hexagonsVertices.Count);
                        hexagonsVertices.Enqueue(newVert);
                        float[] biomesWeights = GetVertexBiomesNoisedWeigths(newVert);
                        TerrainVertex terrainVertex = new TerrainVertex(hexagonTerrainVertices.Count, newVert);
                        terrainVertex.biomesWeights = biomesWeights;
                        hexagonTerrainVertices.Enqueue(terrainVertex);
                        if (k >= startingCircleIndex)
                        {
                            outerCircleVerticesList.Add(terrainVertex);
                        }
                    }
                }

                _hexagonCells[index].verticesIndices = newCellVerticesIndices.ToArray();
                if (i != 0 || j != 0)
                {
                    for (int k = 0; k < baseTriangles.Length; k++)
                    {
                        hexagonTriangles.Enqueue(cellVerticesAliases[baseTriangles[k]]);
                    }
                }
            }
        }


        _terrainVertices = hexagonTerrainVertices.ToArray();
        _terrainMesh = new Mesh();
        _terrainMesh.name = "HexTerrain";
        _terrainMesh.vertices = hexagonsVertices.ToArray();
        _terrainMesh.triangles = hexagonTriangles.ToArray();
        _terrainMesh.RecalculateNormals();
        Color[] verticesColors = new Color[_terrainMesh.vertices.Length];

        for (int i = 0; i < _terrainMesh.vertices.Length; i++)
        {
            Color blendedColor = Color.black;
            for (int j = 0; j < _islandBiomeCores.Length; j++)
            {
                if (_terrainVertices[i].biomesWeights[j] > 0)
                {

                    Color color = biomeColors[_islandBiomeCores[j].biomeType];

                    blendedColor += color * _terrainVertices[i].biomesWeights[j];
                }
            }

            verticesColors[i] = blendedColor;
            float distanceFromCenter = _terrainMesh.vertices[i].sqrMagnitude;
        }

        _terrainMesh.colors = verticesColors;
        GetComponent<MeshFilter>().mesh = _terrainMesh;
        GetComponent<MeshRenderer>().material = _mainMaterial;
        GetComponent<MeshCollider>().sharedMesh = _terrainMesh;
        TerrainDrew?.Invoke();

    }
    public override void AssignNeighbours()
    {
        for (int i = 0; i < _terrainVertices.Length; i++)
        {
            _terrainVertices[i].neighbourIndices = GetNeighbours(_terrainVertices[i]);
        }
    }

    public int[] GetNeighbours(TerrainVertex vertex)
    {
        if (vertex.index == 0)
        {
            int[] neighbours = new int[6];
            for (int j = 0; j < 6; j++)
            {
                neighbours[j] = j + 1;
            }

            return neighbours;
        }

        Queue<int> neighboursQueue = new Queue<int>();
        int ringStartIndex = GetTriangularRingStartingIndex(vertex.triangularRingLevel);
        int ringLength = 6 * vertex.triangularRingLevel;
        int lowerRingLength = 6 * (vertex.triangularRingLevel - 1);

        if (vertex.isAngular)
        {
            neighboursQueue.Enqueue(vertex.index + 1);
            neighboursQueue.Enqueue(vertex.exantIndex != 0 ? (vertex.index - 1) : (vertex.index - 1) + ringLength);
            neighboursQueue.Enqueue(vertex.triangularRingLevel - 1 == 0 ? 0 : vertex.index - (lowerRingLength + vertex.exantIndex));
            if (!vertex.isExternal)
            {
                int upperRingLength = 6 * (vertex.triangularRingLevel + 1);
                int directUpperRingIndex = vertex.index + ringLength + vertex.exantIndex;
                neighboursQueue.Enqueue(directUpperRingIndex);
                neighboursQueue.Enqueue(directUpperRingIndex + 1);
                neighboursQueue.Enqueue(vertex.exantIndex != 0 ? directUpperRingIndex - 1 : directUpperRingIndex - 1 + upperRingLength);
            }
        }
        else
        {
            neighboursQueue.Enqueue(vertex.index + 1 < ringStartIndex + ringLength ? vertex.index + 1 : ringStartIndex);
            neighboursQueue.Enqueue(vertex.index - 1);
            int directLowerRingIndex = vertex.index - (lowerRingLength + vertex.exantIndex);
            neighboursQueue.Enqueue(directLowerRingIndex);
            neighboursQueue.Enqueue(directLowerRingIndex - 1);
            if (!vertex.isExternal)
            {
                int directUpperRingIndex = vertex.index + ringLength + vertex.exantIndex;
                neighboursQueue.Enqueue(directUpperRingIndex);
                neighboursQueue.Enqueue(directUpperRingIndex + 1);
            }
        }

        return neighboursQueue.ToArray();
    }

    public int GetTriangularRingStartingIndex(int triangularRingLevel)
    {
        int index = 0;
        if (triangularRingLevel == 0)
        {
            return index;
        }

        for (int i = 1; i < triangularRingLevel; i++)
        {
            index += 6 * i;
        }

        return index + 1;
    }
    public int[] GetVerticesInsideRadius(int vertexIndex, float radius)
    {
        Queue<int> sorroundingVerticesIndices = new Queue<int>();
        sorroundingVerticesIndices.Enqueue(vertexIndex);
        HashSet<int> exploredVerticesMap = new HashSet<int>() { vertexIndex };
        float sqrRadius = radius * radius;
        Vector3 vertexPosition = _terrainVertices[vertexIndex].position;
        Vector2 vertexPosition2D = new Vector2(vertexPosition.x, vertexPosition.z);
        int circles = Mathf.FloorToInt(radius / _verticesDistance);
        int explorationCap = 1;
        for (int i = 1; i <= circles + 1; i++)
        {
            explorationCap += 6 * i;
        }

        Queue<int> newIndices = new Queue<int>();
        newIndices.Enqueue(vertexIndex);
        while (exploredVerticesMap.Count < explorationCap && exploredVerticesMap.Count < _terrainVertices.Length)
        {
            int[] previousIndices = newIndices.ToArray();
            newIndices = new Queue<int>();
            for (int i = 0; i < previousIndices.Length; i++)
            {
                int[] neighbours = _terrainVertices[previousIndices[i]].neighbourIndices;
                for (int j = 0; j < neighbours.Length; j++)
                {
                    if (!exploredVerticesMap.Contains(neighbours[j]))
                    {
                        Vector3 newVertexPosition;
                        newVertexPosition = _terrainVertices[neighbours[j]].position;
                        if ((new Vector2(newVertexPosition.x, newVertexPosition.z) - vertexPosition2D).sqrMagnitude <= sqrRadius)
                        {
                            sorroundingVerticesIndices.Enqueue(neighbours[j]);
                        }

                        newIndices.Enqueue(neighbours[j]);
                        exploredVerticesMap.Add(neighbours[j]);
                    }
                }
            }
        }

        return sorroundingVerticesIndices.ToArray();
    }

    #region BIOMES
    public BiomeType[] GetBiomes(bool allowRepeatedBiome = false)
    {
        UnityEngine.Random.InitState(seed);
        int biomeNumber = /*(int)_islandRarity +*/ 1;
        List<BiomeType> biomesList = new List<BiomeType>();
        BiomeType[] biomes = new BiomeType[biomeNumber];
        for (int i = 0; i < biomeNumber; i++)
        {
            int random = UnityEngine.Random.Range(0, 5);
            if (!allowRepeatedBiome)
            {
                while (biomesList.Contains((BiomeType)random))
                {
                    random = (random + 1) % 5;
                }

                biomesList.Add((BiomeType)random);
            }
        }

        biomes = biomesList.ToArray();
        Array.Sort(biomes);
        return biomes;
    }

    public static BiomeType[] GetBiomes(int seed, int biomesCount, bool allowRepeatedBiome = false)
    {
        UnityEngine.Random.InitState(seed);
        List<BiomeType> biomesList = new List<BiomeType>();
        BiomeType[] biomes = new BiomeType[biomesCount];
        for (int i = 0; i < biomesCount; i++)
        {
            int random = UnityEngine.Random.Range(0, 5);
            if (!allowRepeatedBiome)
            {
                while (biomesList.Contains((BiomeType)random))
                {
                    random = (random + 1) % 5;
                }

                biomesList.Add((BiomeType)random);
            }
        }

        biomes = biomesList.ToArray();
        Array.Sort(biomes);
        return biomes;
    }
    private void PlaceBiomeCores()
    {
        BiomeType[] biomes = GetBiomes();
        _islandBiomeCores = new BiomeCore[biomes.Length];
        for (int i = 0; i < biomes.Length; i++)
        {
            Vector3 corePosition = (Quaternion.Euler(0, i * 360f / biomes.Length, 0) * Vector3.forward) * _islandRadius;
            _islandBiomeCores[i].localPosition = corePosition;
            _islandBiomeCores[i].biomeType = biomes[i];
        }
    }

    private static BiomeType[] GetClosestBiomes(Vector3 position, BiomeCore[] cores)
    {
        float minCoreDistance = Mathf.Infinity;
        List<BiomeType> closestBiomeTypes = new List<BiomeType>();

        if (cores != null)
        {
            for (int i = 0; i < cores.Length; i++)
            {
                float coreDistance = Vector2.SqrMagnitude(new Vector2(cores[i].localPosition.x, cores[i].localPosition.z) - new Vector2(position.x, position.z));
                if (coreDistance < minCoreDistance)
                {
                    closestBiomeTypes = new List<BiomeType>(new BiomeType[] { cores[i].biomeType });
                    minCoreDistance = coreDistance;
                }
                else if (Mathf.Approximately(coreDistance, minCoreDistance))
                {
                    closestBiomeTypes.Add(cores[i].biomeType);
                }
            }
        }

        return closestBiomeTypes.ToArray();
    }
    public static BiomeType GetMainBiome(float[] weights)
    {
        float maxWeight = -1f;
        int maxIndex = 0;
        for (int i = 0; i < weights.Length; i++)
        {
            if (weights[i] > maxWeight)
            {
                maxWeight = weights[i];
                maxIndex = i;
            }
        }
        return (BiomeType)maxIndex;
    }

    private BiomeType GetClosestBiome(float[] weights)
    {
        float maxWeight = 0;
        BiomeType closestBiomeType = BiomeType.DESERT;
        if (_islandBiomeCores != null)
        {
            for (int i = 0; i < _islandBiomeCores.Length; i++)
            {
                if (weights[i] > maxWeight)
                {
                    closestBiomeType = _islandBiomeCores[i].biomeType;
                    maxWeight = weights[i];
                }
            }
        }

        return closestBiomeType;
    }

    private BiomeType GetClosestBiomeWithNoise(Vector3 position)
    {
        float minCoreDistance = Mathf.Infinity;
        BiomeType closestBiomeType = BiomeType.DESERT;
        float offset = RemapValue(Mathf.PerlinNoise((position.x + _seededOffset) * _biomeNoiseLacunarity, (position.z + _seededOffset) * _biomeNoiseLacunarity), 0, 1, -_biomeNoiseAmplitude / 2, _biomeNoiseAmplitude / 2);
        position += Quaternion.Euler(0, 90, 0) * (new Vector3(position.x, 0, position.z) * offset);
        if (_islandBiomeCores != null)
        {
            for (int i = 0; i < _islandBiomeCores.Length; i++)
            {
                float coreDistance = Vector2.SqrMagnitude(new Vector2(_islandBiomeCores[i].localPosition.x, _islandBiomeCores[i].localPosition.z) - new Vector2(position.x, position.z));
                if (coreDistance < minCoreDistance)
                {
                    closestBiomeType = _islandBiomeCores[i].biomeType;
                    minCoreDistance = coreDistance;
                }
            }
        }

        return closestBiomeType;
    }

    private float[] GetVertexBiomesNoisedWeigths(Vector3 position)
    {
        float[] weights = new float[_islandBiomeCores.Length];
        float weightSum = 0;
        float offset = RemapValue(Mathf.PerlinNoise((position.x + _seededOffset) * _biomeNoiseLacunarity, (position.z + _seededOffset) * _biomeNoiseLacunarity), 0, 1, -_biomeNoiseAmplitude / 2, _biomeNoiseAmplitude / 2);
        position += Quaternion.Euler(0, 90, 0) * (new Vector3(position.x, 0, position.z) * offset);
        if (_islandBiomeCores != null)
        {
            for (int i = 0; i < _islandBiomeCores.Length; i++)
            {
                weights[i] = 1f / Vector2.SqrMagnitude(new Vector2(_islandBiomeCores[i].localPosition.x, _islandBiomeCores[i].localPosition.z) - new Vector2(position.x, position.z));
                weightSum += weights[i];
            }

            float inversedWeightSum = 1 / weightSum;
            weightSum = 0;
            for (int i = 0; i < _islandBiomeCores.Length; i++)
            {
                weights[i] = weights[i] * inversedWeightSum;
                if (weights[i] < BIOMEWEIGHTBLENDLOWERBOUND)
                {
                    weights[i] = 0;
                }
                else
                {
                    weights[i] *= Mathf.InverseLerp(BIOMEWEIGHTBLENDLOWERBOUND, BIOMEWEIGTHBLENDUPPERBOUND, weights[i]);
                }

                weightSum += weights[i];
            }

            inversedWeightSum = 1 / weightSum;
            for (int i = 0; i < _islandBiomeCores.Length; i++)
            {
                weights[i] = weights[i] * inversedWeightSum;
            }
        }

        return weights;
    }
    #endregion

    protected float CalculateVertexHeight(float x, float z, bool useXForDistance = false, float radius = 0)
    {
        switch (currentTerrain)
        {
            case TerrainType.PLATEAU:
                return CalculatePlateauHeight(x, z);
            case TerrainType.ISLAND:
                return CalculateIslandHeightWithShoreNoise(x, z, useXForDistance, radius);
            default:
                return 0;
        }
    }

    #region PLATEAU
    public float CalculatePlateauHeight(float x, float z)
    {
        Vector2 vertexPosition = new Vector2(x, z);
        float height = CalculateHeightWithNoiseOctaves(x, z);
        //Mathf.PerlinNoise((x + _seededOffset) * _fjordFrequency, (z + _seededOffset) * _fjordFrequency) * _maxHeight;
        return height;
    }

    float CalculateHeightWithNoiseOctaves(float x, float y)
    {
        float value = 0;
        float octaveMaxHeight = _maxHeight;
        float octaveFrequency = _noiseBaseFrequency;

        for (int i = 0; i < _octaves; i++)
        {
            value += octaveMaxHeight * Mathf.PerlinNoise((x + _seededOffset) * octaveFrequency, (y + _seededOffset) * octaveFrequency);
            octaveMaxHeight *= _persistence;
            octaveFrequency *= _lacunarity;
        }

        return value;
    }
    #endregion

    #region Island

    public float CalculateIslandHeight(float distanceFromCenter, float internalRadius)
    {
        float height = 0;
        float shoreDistance = internalRadius + _beachLength;
        float seabedDistance = shoreDistance + _seaBedShoreDistance;
        if (distanceFromCenter > shoreDistance)
        {
            height = -Mathf.InverseLerp(shoreDistance, seabedDistance, distanceFromCenter) * _seaBedDepth;
        }
        else
        {
            float _beachSmoothing = 1 - Mathf.InverseLerp(internalRadius, shoreDistance, distanceFromCenter);
            height = (_maxHeight * _beachSmoothing);
        }

        return height;
    }

    public float CalculateIslandHeightWithShoreNoise(float x, float z, bool useXForDistance = false, float radius = 0)
    {
        Vector2 meshCenter = Vector2.zero;
        Vector2 vertexPosition = new Vector2(x, z);
        float distanceFromCenter = useXForDistance ? Mathf.Abs(x) : Vector2.Distance(meshCenter, vertexPosition);
        radius = (radius == 0) ? _islandRadius : radius;
        float beachMaxOffset = radius * _beachMaxOffsetRatio;
        float randomShoreOffset = RemapValue(Mathf.PerlinNoise((x + _seededOffset) * _fjordFrequency, (z + _seededOffset) * _fjordFrequency), 0, 1, -beachMaxOffset, beachMaxOffset);
        float noisedIslandRadius = radius - (beachMaxOffset) + randomShoreOffset;
        return CalculateIslandHeight(distanceFromCenter, noisedIslandRadius);
    }

    public float CalculateIslandConnectionHeightWithNoise(float x, float z, float width)
    {
        Vector2 meshCenter = Vector2.zero;
        Vector2 vertexPosition = new Vector2(x, z);
        float distanceFromCenter = Mathf.Abs(x);
        float beachMaxOffset = width * _beachMaxOffsetRatio;
        float taperingStartingDistance = _islandCellTriangleHeight / 2;
        float randomShoreOffset = RemapValue(Mathf.PerlinNoise((x + _seededOffset) * _fjordFrequency * 2, (z + _seededOffset) * _fjordFrequency * 2), 0, 1, -beachMaxOffset, beachMaxOffset);
        float noisedWidth = width - (beachMaxOffset) + randomShoreOffset;
        float tapering = (z - taperingStartingDistance) / (_islandCellTriangleHeight - taperingStartingDistance);
        float taperedWidth = Mathf.Lerp(noisedWidth, width, tapering);
        return CalculateIslandHeight(distanceFromCenter, taperedWidth);
    }
    #endregion

    #region LEGACY
    public void CalculateQuadsTerrain()
    {
        UnityEngine.Random.InitState(seed);
        PlaceBiomeCores();
        _seededOffset = UnityEngine.Random.Range(0, MAXNOISEOFFSET);
        _verticesDistance = 1 / _verticesDensity;
        _gridSize = (int)Mathf.Round(_terrainSize * _verticesDensity);
        _halfSize = _terrainSize / 2;
        _meshVertices = new Vector3[(_gridSize + 1) * (_gridSize + 1)];
        _terrainVertices = new TerrainVertex[(_gridSize + 1) * (_gridSize + 1)];
        _heights = new float[(_gridSize + 1) * (_gridSize + 1)];
        Vector3 startingPoint = new Vector3(-_halfSize, 0, -_halfSize);
        _uv = new Vector2[(_gridSize + 1) * (_gridSize + 1)];
        for (int i = 0, row = 0; row < (_gridSize + 1); row++)
        {
            for (int column = 0; column < (_gridSize + 1); column++, i++)
            {
                float coordX = startingPoint.x + column * _verticesDistance;
                float coordZ = startingPoint.z + row * _verticesDistance;
                float height = CalculateIslandHeightWithShoreNoise(coordX, coordZ);
                _heights[i] = height;
                _meshVertices[i] = new Vector3(coordX, height, coordZ);
                _terrainVertices[i].position = _meshVertices[i];
                _terrainVertices[i].index = i;
                _uv[i] = new Vector2((float)column / _gridSize, (float)row / _gridSize);
            }
        }

        _terrainMesh = new Mesh();
        _terrainMesh.name = "Terrain";
        _terrainMesh.vertices = _meshVertices;
        _terrainMesh.uv = _uv;

        int[] triangles = TriangulateGrid();

        _terrainMesh.triangles = triangles;
        _terrainMesh.RecalculateNormals();

        Color[] verticesColors = new Color[_terrainMesh.vertices.Length];
        //for (int i = 0; i < _terrainMesh.vertices.Length; i++)
        //{
        //    Color color = biomeColors[_terrainVertices[i].biomeType];
        //    verticesColors[i] = color;
        //}

        _terrainMesh.colors = verticesColors;
        GetComponent<MeshFilter>().mesh = _terrainMesh;
        GetComponent<MeshRenderer>().material = _mainMaterial;
        GetComponent<MeshCollider>().sharedMesh = _terrainMesh;
        TerrainDrew?.Invoke();
    }
    #endregion
}

[Serializable]
public struct BiomeColor
{
    public BiomeType type;
    public Color color;
}

[Serializable]
public struct BiomePostProcessingProfile
{
    public BiomeType type;
    public VolumeProfile profile;
}
[Serializable]
public struct HexagonCell
{
    public Vector3 centerOffset;
    public int[] verticesIndices;
    public BiomeType mainBiome;

    public HexagonCell(Vector3 centerOffset, int[] verticesIndices = null, BiomeType mainBiome = BiomeType.DESERT)
    {
        this.centerOffset = centerOffset;
        this.verticesIndices = verticesIndices;
        this.mainBiome = mainBiome;
    }
}

public enum TerrainType
{
    PLATEAU,
    ISLAND
}

