using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(BaseTerrainGenerator))]
public class RainEroder : MonoBehaviour
{
    protected const float FIRST_DROP_CONTACT_SLOPE_MULTIPLIER = 1f;
    [SerializeField]
    protected float sedimentDropSlopeRatio = 0.0001f;
    protected BaseTerrainGenerator _generator;
    [SerializeField]
    protected int erodingDropsCount = 1;
    private void OnValidate()
    {
        _generator = GetComponent<BaseTerrainGenerator>();
    }

    public void Erode()
    {
        float time = Time.realtimeSinceStartup;
        Vector3[] vertices = _generator.TerrainMesh.vertices;
        _generator.AssignNeighbours();
        for (int i = 0; i < erodingDropsCount; i++)
        {
            RainDrop(ref vertices);
        }

        time = Time.realtimeSinceStartup - time;
        Debug.Log("Erode time: " + time);
        _generator.DrawMesh(vertices, _generator.TerrainMesh.triangles);
        _generator.ApplyMeshToRenderAndCollider();
        _generator.ColorMesh();
    }
    public void RainDrop(ref Vector3[] meshPoints)
    {
        float sqrVerticesDistance = (meshPoints[0] - meshPoints[1]).sqrMagnitude;
        Queue<int> modifiedIndices = new Queue<int>();
        //Drop drop = new Drop(0f, DROP_TERMINAL_SPEED);
        float finalSediment = 0;
        float sediment = 0;
        int startingVertexIndex = Random.Range(0, _generator.TerrainVertices.Length);
        Vector3 startingVertexPosition = _generator.TerrainVertices[startingVertexIndex].position;
        sediment = sedimentDropSlopeRatio * FIRST_DROP_CONTACT_SLOPE_MULTIPLIER;// * (1 - CalculateNeighboursNormalsAffinity(startingVertexIndex));
        finalSediment += sediment;
        meshPoints[startingVertexIndex] -= Vector3.up * sediment;
        modifiedIndices.Enqueue(startingVertexIndex);
        int nextVertexIndex = FindNextVertexIndex(startingVertexIndex);
        Vector3 nextVertexPosition = _generator.TerrainVertices[nextVertexIndex].position;
        while (startingVertexIndex != nextVertexIndex)
        {
            Vector3 fromToVector = nextVertexPosition - startingVertexPosition;
            float distance = Mathf.Sqrt(sqrVerticesDistance + fromToVector.y * fromToVector.y);
            float slopeSqrt = Mathf.Sqrt(-fromToVector.y / distance);
            //float duration = GetSimplifiedPassageTime(slope, drop.speed, distance);
            //float endSpeed = (2 * distance / duration) - drop.speed;
            //drop.speed = endSpeed;
            sediment = sedimentDropSlopeRatio * slopeSqrt;// * (1 - CalculateNeighboursNormalsAffinity(nextVertexIndex));
            finalSediment += sediment;
            meshPoints[nextVertexIndex] -= Vector3.up * sediment;
            modifiedIndices.Enqueue(nextVertexIndex);
            int newIndex = FindNextVertexIndex(nextVertexIndex);
            startingVertexIndex = nextVertexIndex;
            startingVertexPosition = nextVertexPosition;
            nextVertexIndex = newIndex;
            nextVertexPosition = _generator.TerrainVertices[nextVertexIndex].position;
            //Debug.Log("Drop passing on " + startingVertexIndex + /*" at " + endSpeed + " m/s and remove "*/" remove " + sediment + " sediment.");
        }

        meshPoints[nextVertexIndex] += Vector3.up * finalSediment;
        foreach (var index in modifiedIndices)
        {
            _generator.TerrainVertices[index].position = meshPoints[index];
        }
        //Debug.Log("Drop reach on " + startingVertexIndex + " and leaves " + finalSediment + " sediment.");
    }

    //protected float GetSimplifiedPassageTime(float slope, float startingSpeed, float distance)
    //{
    //    float a = GRAVITY_MODULE * slope - startingSpeed * DYNAMIC_DRAG * 0.5f;
    //    float b = 2 * startingSpeed;
    //    float c = -2 * distance;
    //    return (-b + Mathf.Sqrt(b * b - 4 * a * c)) / 2 * a;
    //}

    //protected float GetPassageTime(float slope, float startingSpeed, float distance)
    //{
    //    float a = GRAVITY_MODULE * slope;
    //    float b = 2 * startingSpeed - DYNAMIC_DRAG * distance;
    //    float c = -2 * distance;
    //    return (-b + Mathf.Sqrt(b * b - 4 * a * c)) / 2 * a;
    //}

    protected float DropPassage(TerrainVertex vertex, float dropSpeed)
    {
        float sediment = sedimentDropSlopeRatio * dropSpeed;
        return sediment;
    }

    private TerrainVertex FindNextVertex(TerrainVertex vertex)
    {
        TerrainVertex nextVertex = vertex;
        float minHeight = vertex.position.y;
        for (int i = 0; i < vertex.neighbourIndices.Length; i++)
        {
            int neighbourIndex = vertex.neighbourIndices[i];
            TerrainVertex neighbourVertex = _generator.TerrainVertices[neighbourIndex];
            if (minHeight > neighbourVertex.position.y)
            {
                nextVertex = neighbourVertex;
                minHeight = neighbourVertex.position.y;
            }
        }

        return nextVertex;
    }
    private int FindNextVertexIndex(int index)
    {
        TerrainVertex vertex = _generator.TerrainVertices[index];
        float minHeight = vertex.position.y;
        int lowerNeighbourIndex = index;
        for (int i = 0; i < vertex.neighbourIndices.Length; i++)
        {
            int newIndex = vertex.neighbourIndices[i];
            TerrainVertex neighbourVertex = _generator.TerrainVertices[newIndex];

            if (minHeight > neighbourVertex.position.y)
            {
                minHeight = neighbourVertex.position.y;
                lowerNeighbourIndex = newIndex;
            }
        }

        return lowerNeighbourIndex;
    }

    private float CalculateNeighboursNormalsAffinity(int vertexIndex)
    {
        int[] neighboursIndices = _generator.TerrainVertices[vertexIndex].neighbourIndices;
        Vector3 neighboursNormal = Vector3.zero;
        for (int i = 0; i < neighboursIndices.Length; i++)
        {
            neighboursNormal += _generator.TerrainMesh.normals[neighboursIndices[i]];
        }

        neighboursNormal /= neighboursIndices.Length;
        return Vector3.Dot(_generator.TerrainMesh.normals[vertexIndex], neighboursNormal);
    }
}

public class Drop
{
    public float sediment;
    public float speed;

    public Drop(float initialSediment, float initialSpeed)
    {
        sediment = initialSediment;
        speed = initialSpeed;
    }
}
